//function counterFactory() {
  // Return an object that has two methods called `increment` and `decrement`.
  // `increment` should increment a counter variable in closure scope and return it.
  // `decrement` should decrement the counter variable and return it.
//}
function counterFactory() {
  var count=0;
  function increment(){return ++count};
  function decrement(){return --count};

  return {increment,decrement};
}
console.log(counterFactory().increment())


//function limitFunctionCallCount(cb, n) {
  // Should return a function that invokes `cb`.
  // The returned function should only allow `cb` to be invoked `n` times.
  // Returning null is acceptable if cb can't be returned
//}

const cb=function(){console.log("invoked in cb")};
function limitFunctionCallCount(cb, n) {
  var count=n;
  function invoke()
  {
    if(count>0)
    {
      count--;
    cb();
    }
  }
  return {invoke};
}
var a=limitFunctionCallCount(cb, 3);
a.invoke()
a.invoke()
a.invoke()
a.invoke()
a.invoke()



//function cacheFunction(cb) {
  // Should return a funciton that invokes `cb`.
  // A cache (object) should be kept in closure scope.
  // The cache should keep track of all arguments have been used to invoke this function.
  // If the returned function is invoked with arguments that it has already seen
  // then it should return the cached result and not invoke `cb` again.
  // `cb` should only ever be invoked once for a given set of arguments.
//}
function cb(val){console.log("invoke cb="+val) };
  function cacheFunction(cb) {
  var cache={};
  var count=1;
  function invoke(val)
  {
    console.log()
    for(i in cache)
    {
     if(cache[i]==val)
        {
          return cache
        }
    }
    cache[count]=val;
    count++;
    cb(val)
  }
  return {invoke}
  
}
var fun=cacheFunction(cb)
fun.invoke(2)
fun.invoke(3)
fun.invoke(2)
