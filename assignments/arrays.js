const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code. 
/*
  Complete the following functions.
  These functions only need to work with arrays.
  A few of these functions mimic the behavior of the `Built` in JavaScript Array Methods.
  The idea here is to recreate the functions from scratch BUT if you'd like,
  feel free to Re-use any of your functions you build within your other functions.
  **DONT** Use for example. .forEach() to recreate each, and .map() to recreate map etc.
  You CAN use concat, push, pop, etc. but do not use the exact method that you are replicating
*/

//function each(elements, cb) {
  // Do NOT use forEach to complete this function.
  // Iterates over a list of elements, yielding each in turn to the `cb` function.
  // This only needs to work with arrays.
  // You should also pass the index into `cb` as the second argument
  // based off http://underscorejs.org/#each
//}

/**each */
const cb=function(item,index)
{
  console.log(item);
}
function each(elements, cb) {
for(let i=0;i<elements.length;i++)
{
  cb(elements[i],i);
}
}
each(items,cb)

//function map(elements, cb) {
  // Do NOT use .map, to complete this function.
  // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
  // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
  // Return the new array.
//}

/**map */
const cb=function(item,index)
{
  return (2*item);
}
function map(elements, cb) {
  const myarr=[]
for(let i=0;i<elements.length;i++)
{
  myarr.push(cb(elements[i],i));
}
return myarr;
}
console.log(map(items,cb))


//function reduce(elements, cb, startingValue) {
  // Do NOT use .reduce to complete this function.
  // How reduce works: A reduce function combines all elements into a single value going from left to right.
  // Elements will be passed one by one into `cb` along with the `startingValue`.
  // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
  // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
//}

/**reduce */
const cb=function(accumulator,item)
{
  return item+accumulator;
}
function reduce(elements, cb, startingValue)  {
  var sum=0,i=1;
  if(startingValue===undefined)
  {
    startingValue= elements[0];
    i++;
  }
for(i;i<=elements.length;i++)
{
  startingValue=cb(startingValue,elements[i-1]);
}
return startingValue;
  }

console.log(reduce(items,cb,4))


//function find(elements, cb) {
  // Do NOT use .includes, to complete this function.
  // Look through each value in `elements` and pass each element to `cb`.
  // If `cb` returns `true` then return that element.
  // Return `undefined` if no elements pass the truth test.
//}

/** find*/
const cb=function(item)
{
  if(item===2)
  {
    return true;
  }
}

function find(elements, cb)  {
  var result=undefined;
  for(let i=0;i<elements.length;i++)
  {
  if(cb(elements[i]))
  {
    return elements[i]
  }
  }
  return result;
  
}
console.log(find(items,cb))



//function filter(elements, cb) {
  // Do NOT use .filter, to complete this function.
  // Similar to `find` but you will return an array of all elements that passed the truth test
  // Return an empty array if no elements pass the truth test
//}

/** filter*/
const cb=function(item)
{
  if(item>2)
  {
    return true;
  }
}
function filter(elements, cb)  {
  var result=[];
  for(let i=0;i<elements.length;i++)
  {
  if(cb(elements[i]))
  {
    result.push(elements[i])
  }
  }
  return result;
  
}
console.log(filter(items,cb))


const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

//function flatten(elements) {
  // Flattens a nested array (the nesting can be to any depth).
  // Hint: You can solve this using recursion.
  // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
//}

function flatten(elements)
{
  let flag=0,i=0;
  let mylen=elements.length;
   const stack = [...elements];
  while(1)
  {
  for(let i=0;i<mylen;i++)
  {
    var shiftVal=stack.shift();
    if(Array.isArray(shiftVal))
    {
      stack.push(...shiftVal);
      flag=1;
    }
    else
    {
      stack.push(shiftVal)
    }
  console.log(stack)
  }
  if(flag==0)
  {
    break;
  }
  flag=0;
  }
  return stack;
}

console.log(flatten(nestedArray))

