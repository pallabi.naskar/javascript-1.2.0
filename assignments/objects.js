const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.


  // Retrieve all the names of the object's properties.
  // Return the keys as strings in an array.
  // Based on http://underscorejs.org/#keys

function keys(obj)
   {
    const myarr=[];
    for(var key in obj)
     {
       myarr.push(key)
    }
    return myarr
    } 
keys(testObject)



  // Return all of the values of the object's own properties.
  // Ignore functions
  // http://underscorejs.org/#values

  function values(obj) 
  {
      const myarr=[];
    for(var key in obj) 
    {
        myarr.push(obj[key])
    }
     return myarr
    } 
    values(testObject)
 


//function mapObject(obj, cb) {
  // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
  // http://underscorejs.org/#mapObject
//}
const cb=function(key,key_val){
  return key_val+"New";
}

function mapObject(obj, cb) {

 for(var key in obj) 
 {
     obj[key]=cb(key,obj[key])
 }
  return obj
}
mapObject(testObject, cb)




  // Convert an object into a list of [key, value] pairs.
  // http://underscorejs.org/#pairs
  function pairs(obj){
    const myarr=[];
      for(var key in obj) 
      {
        let subarr=[]
        subarr.push(key,obj[key])
          myarr.push(subarr)
      }
       return myarr
    }
    pairs(testObject) 
  



/* STRETCH PROBLEMS */

  // Returns a copy of the object where the keys have become the values and the values the keys.
  // Assume that all of the object's values will be unique and string serializable.
  // http://underscorejs.org/#invert
  function invert(obj){
    var myObj={};
    const myarr=[];
      for(var key in obj) 
      {
        myarr.push(obj[key],key)
      }
    for(let i=0;i<myarr.length;i+=2)
    {
      myObj[myarr[i]]=myarr[i+1]
    }
       return myObj
    }
    invert(testObject) 




  // Fill in undefined properties that match properties on the `defaultProps` parameter object.
  // Return `obj`.
  // http://underscorejs.org/#defaults
  function defaults(obj, defaultProps) {
    for(i in defaultProps)
    {
      obj[i]=defaultProps[i];
    }
     return obj; 
}

console.log(defaults(testObject,{name:"pallabi",roll:28}))

